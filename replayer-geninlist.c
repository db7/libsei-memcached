#include <stdint.h>
#include <string.h>
#include <stdio.h>
typedef struct {
    uint32_t crc;
    char* data;
} rmsg_t;

#include "replayer-input.h"

#define S sizeof(inlist) / sizeof (rmsg_t)
#define CRC_INIT 0xFFFFFFFF

uint32_t crc_compute(const char* block, size_t len);

int
main(const int argc, const char* argv[])
{
    int i,j;
    size_t len;
    printf("rmsg_t inlist[] = {\n");
    for (i = 0; i < S; ++i) {
        if (i > 0) printf(",\n");
        len = strlen(inlist[i].data);
        uint32_t crc = crc_compute(inlist[i].data, len-2);
        printf("\t{.crc = %u, .data = \"", crc);
        for (j = 0; j < len; ++j) {
            char c = inlist[i].data[j];
            if (c == '\n') printf("\\n");
            else if (c == '\r') printf("\\r");
            else printf("%c", c);
        }
        printf("\"}");
     }
    printf("\n};\n");
    return 0;
}

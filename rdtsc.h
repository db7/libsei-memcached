#ifndef _RDSTC_H_
#define _RDSTC_H_

#include <stdint.h>

#if !defined(__x86_64__)
#error only x64 supported
#endif

static inline uint64_t
rdtsc(void)
{
    uint64_t a, d;
    __asm__ __volatile__ ("rdtsc" : "=a"(a), "=d"(d));
    return (d<<32) | a;
}

#endif /* _RDSTC_H_ */

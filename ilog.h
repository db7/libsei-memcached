#ifndef _ILOG_H_
#define _ILOG_H_
typedef struct ilog ilog_t;
ilog_t* ilog_init(const char*);
void ilog_fini(ilog_t*);
void ilog_push(ilog_t*, const char*, const char*);
#endif

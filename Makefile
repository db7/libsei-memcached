##############################################################################
# use GCC 4.7
CC         = gcc-4.7

# Minimal flags
CFLAGS     = -g -std=gnu99 -fno-builtin -fgnu-tm -DSLABS_DISABLED
CFLAGS_OPT = -O3 -fno-omit-frame-pointer -U_FORTIFY_SOURCE -DNDEBUG
CFLAGS_DBG = -O0

# Uncomment to disable SEI
# CFLAGS    += -DSEI_DISABLED 

# Uncomment to use checking barrier
# CFLAGS    += -DSEI_TBAR

# Uncomment to compile replayer
# CFLAGS    += -DREPLAYER 

# Select debug (CFLAGS_DBG) or optimized (CFLAGS_OPT) compilation
CFLAGS    += $(CFLAGS_OPT)

# Use trylock by default (according to paper's Evaluation)
CFLAGS    += -DTRYLOCK_ITEM_UPDATE

##############################################################################
SRCS   = glue.c
BUILD  = build
OBJS   = $(addprefix $(BUILD)/, $(SRCS:.c=.o))
TARGET = memcached.sei

# Libraries and paths
include Makefile.local # define SEI_HOME in Makefile.local 
INCP   = -I$(SEI_HOME)/include
LIBP   = -L$(SEI_HOME)/build
LIBS   = -pthread -lsei -levent -ldl -lm 

##############################################################################
.PHONY: all clean

all: $(BUILD)/$(TARGET)

$(BUILD):
	mkdir -p $(BUILD)

$(BUILD)/%.o : %.c | $(BUILD)
	$(CC) $(CFLAGS) $(INCP) -c -o $@ $<

$(BUILD)/$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) $(LIBP) -o $@ $^ $(LIBS)

clean:
	rm -rf $(BUILD)


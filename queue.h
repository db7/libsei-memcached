/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * A lock-free queue for 1 producer and 1 consumer.
 *
 * First appeared in L. Lamport. Specifying concurrent program
 * modules. ACM Transactions on Programming Languages and Systems,
 * 5(2):190–222, 1983.
 *
 * - Added some useful methods such as top() and size() - diogo
 */

#ifndef _QUEUE_H_
#define _QUEUE_H_

typedef struct {
    void** buffer;
    volatile int size;
    volatile int head;
    volatile int tail;
} queue_t;


queue_t* queue_init(int size);
void     queue_fini(queue_t* q);

int      queue_enq(queue_t* q, void* i);
void*    queue_deq(queue_t* q);
void*    queue_top(queue_t* q);
void     queue_pop(queue_t* q);
int      queue_empty(queue_t* q);
int      queue_size(queue_t* q);
int      queue_fsize(queue_t* q);

#define QUEUE_OK     0
#define QUEUE_FULL  -1
#define QUEUE_EMPTY  1
#define QUEUE_NEMPTY 0

#endif /* _QUEUE_H_ */

# SEI-hardened Memcached

A modified version of memcached hardened against data corruptions.

Create a file `Makefile.local` and add the following line to the file:

    SEI_HOME = /path/to/libsei

Then type `make` to compile. The binary is placed in `build/`.

## Dependencies

* libevent, http://www.monkey.org/~provos/libevent/ (libevent-dev)
* libsei, http://bitbucket.org/db7/libsei
* gcc == 4.7


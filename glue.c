#include "config.h"
#include <stdint.h>

#define INLINE_SELECTOR __attribute__((noinline))
#define REPLAYER_MT
#include <sei.h>
#define ptr_t uintptr_t;
#include <limits.h>

#define HAVE_CONFIG_H
#define IOV_MAX 1024
#include "rdtsc.h"
#include <stdint.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/resource.h>

uint64_t outside_;
uint64_t out_start_;
uint64_t out_end_;
uint64_t out_count_;

typedef struct {
    uint64_t inside;
    uint64_t start;
    uint64_t end;
    uint64_t count;
} tsc_t;

tsc_t tsc_process_;
tsc_t tsc_nread_;
tsc_t tsc_others_;

//#define USE_RDTSC
#ifdef USE_RDTSC
#define __rdtsc_start(X) do {                   \
        X.start = rdtsc();                      \
    } while(0)
#define __rdtsc_end(X) do {                     \
        X.end     = rdtsc();                    \
        X.inside += X.end - X.start;            \
        X.count++;                              \
    } while(0)

#define __rdtsc_print(X) do {                                   \
        printf("RDTSC %s %f %lu %lu \n",                        \
               #X, X.inside*1.0/X.count, X.inside, X.count);    \
    } while(0);
#else
#define __rdtsc_start(X)
#define __rdtsc_end(X)
#define __rdtsc_print(X)
#endif

char *strerror(int errnum);
char *strtok(char *str, const char *delim);
char *strtok_r(char *str, const char *delim, char **saveptr);

int fprintf(FILE* stream, const char* fmt, ...) SEI_PURE;
int snprintf(char *str, size_t size, const char *format, ...) SEI_PURE;
void __assert_fail (const char *__assertion, const char *__file, unsigned int __line, const char *__function) SEI_PURE;
size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream) SEI_PURE;
int raise(int sig) SEI_PURE;
int pthread_mutex_lock(pthread_mutex_t *mutex) SEI_PURE;
int pthread_mutex_trylock(pthread_mutex_t *mutex) SEI_PURE;
int pthread_mutex_unlock(pthread_mutex_t *mutex) SEI_PURE;
int pthread_mutex_init(pthread_mutex_t *restrict mutex, const pthread_mutexattr_t *restrict attr) SEI_PURE;
int pthread_mutexattr_init(pthread_mutexattr_t *attr) SEI_PURE;
int pthread_mutexattr_settype(pthread_mutexattr_t *attr, int type) SEI_PURE;
int snprintf(char *str, size_t size, const char *format, ...) SEI_PURE;
int atoi(const char *nptr) SEI_PURE;
void *pthread_getspecific(pthread_key_t key) SEI_PURE;
int pthread_setspecific(pthread_key_t key, const void *value) SEI_PURE;
void abort(void) SEI_PURE;
void perror(const char *s) SEI_PURE;
int pthread_cond_signal(pthread_cond_t *cond) SEI_PURE;
int getrusage(int who, struct rusage *r_usage) SEI_PURE;
pid_t getpid(void) SEI_PURE;
const char *event_get_version(void) SEI_PURE;

/* uint64_t _ITM_RU8(const uint64_t*); */
/* double _ITM_RD(const double* addr) */
/* { return *(double*) _ITM_RU8((const uint64_t*) addr); } */

uint16_t htons(uint16_t hostshort) SEI_PURE;
uint16_t htons(uint16_t n) {
    return (((((unsigned short)(n) & 0xFF)) << 8) | (((unsigned short)(n) & 0xFF00) >> 8));
}

extern uint32_t crc_compute(const char* block, size_t len);
uint32_t
CRC_oracle(void* ptr, size_t size)
{
#if defined(SEI_ENABLED) && !defined(SEI_INSTR)
    return crc_compute(ptr, size);
#else
    return 0;
#endif
}


#ifndef REPLAYER
void
CRC_oracle_tell(uint32_t crc)
{
    // nothing
}
#endif

uint32_t hash(const void *key, size_t length, const uint32_t initval) SEI_PURE;

#ifdef REPLAYER
void exit(int status) SEI_PURE;
#endif

#include "memcached.h"

int
tbar_push(conn* c, void* ptr) {
#ifdef SEI_TBAR
    return queue_enq(c->thread->prioq, ptr);
#else
    return QUEUE_OK;
#endif
}

void
tbar_pop(conn* c) {
#ifdef SEI_TBAR
    queue_pop(c->thread->prioq);
#endif
}

void*
tbar_top(conn* c) {
#ifdef SEI_TBAR
    return queue_top(c->thread->prioq);
#else
    return NULL;
#endif
}

int
tbar_size(conn* c) {
#ifdef SEI_TBAR
    if (c->thread == NULL) return 0;
    return queue_size(c->thread->prioq);
#else
    return 0;
#endif
}


#include "queue.c"
#include "items.c"
#include "assoc.c"
#include "globals.c"
#include "cache.c"
#include "thread.c"
#include "slabs.c"
#include "hash.c"
#include "stats.c"
#include "util.c"
#include "daemon.c"
#ifdef REPLAYER
#include "replayer.c"
#else
#include "memcached.c"
#endif
